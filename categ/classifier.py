# -*- coding: utf-8 -*-
# @Time    : 9/16/18 10:38
# @Author  : Lucien Cho
# @File    : classifier.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import os
import numpy as np
import tensorflow as tf
from sklearn.metrics import f1_score

from .helper import DataLoader, ModeKey, get_params


class Classifier(object):
    def __init__(self, model, model_dir, name, is_training=False):
        params = get_params()
        with tf.variable_scope(name):
            self.model = model(params)
        self.name = name
        self.model_path = os.path.join(model_dir, name, name)
        self.params = self.model.params
        self.tensors = self.model.tensors
        self.session = tf.Session()
        self.saver = tf.train.Saver()
        if not is_training:
            self.saver.restore(self.session, self.model_path)
            tf.reset_default_graph()

    def train(self):
        # Train dependencies
        global_step = tf.Variable(1, trainable=False, dtype=tf.int32)
        learning_rate = tf.train.exponential_decay(
            learning_rate=self.params.learning_rate,
            global_step=global_step,
            decay_steps=100,
            decay_rate=self.params.decay_rate,
            staircase=True)
        optimizer = tf.contrib.opt.LazyAdamOptimizer(learning_rate)
        gvs = optimizer.compute_gradients(self.tensors['loss'])
        capped_gvs = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gvs]
        self.tensors['train_op'] = optimizer.apply_gradients(
            capped_gvs, global_step=global_step, name='train_op')
        init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        self.session.run(init)

        # Data loader
        train_dl = DataLoader(ModeKey.TRAIN, self.params)
        valid_dl = DataLoader(ModeKey.VALID, self.params)
        train_iter = train_dl.batch_iter(self.params.batch_size, self.params.num_epochs)
        valid_iter = valid_dl.batch_iter(2000)
        for n, (train_xs, train_ys) in enumerate(train_iter, start=1):
            self._train_step(train_xs, train_ys, n, self.params.show_step)
            if not n % self.params.save_step:
                valid_xs, valid_ys = valid_iter.__next__()
                self._valid_step(valid_xs, valid_ys, n)
                self.saver.save(self.session, self.model_path)

    def _f1_macro(self, y_trues, y_preds):
        y_trues = np.swapaxes(y_trues, 0, 1)
        y_preds = np.swapaxes(y_preds, 0, 1)
        result = []
        for y_true, y_pred in zip(y_trues, y_preds):
            result.append(f1_score(
                y_true, y_pred, labels=[0, 1, 2, 3], average='macro'))
        return sum(result) / len(result)

    def _train_step(self, xs, ys, step, show_step):
        feed_dict = {self.tensors['inputs']: xs,
                     self.tensors['labels']: ys,
                     self.tensors['keep_prob']: self.params.keep_prob}
        fetches = [self.tensors['logits'], self.tensors['loss'],
                   self.tensors['accuracy'], self.tensors['predictions'],
                   self.tensors['train_op']]
        logits, loss, acc, predictions, _ = self.session.run(fetches, feed_dict)
        f1_macro = self._f1_macro(ys, predictions)
        if step % show_step == 0:
            print('TRAIN | step: {} | loss: {:.3f} | acc: {:.3f} | f1: {:.5f}'.format(
                step, loss, acc[0], f1_macro))

    def _valid_step(self, xs, ys, step):
        feed_dict = {self.tensors['inputs']: xs,
                     self.tensors['labels']: ys,
                     self.tensors['keep_prob']: 1.0}
        fetches = [self.tensors['loss'], self.tensors['accuracy'],
                   self.tensors['predictions']]
        loss, acc, predictions = self.session.run(fetches, feed_dict)
        f1_macro = self._f1_macro(ys, predictions)
        print('\nVALID | step: {} | loss: {:.3f} | acc: {:.3f} | f1: {:.5f}\n'.format(
            step, loss, acc[0], f1_macro))
