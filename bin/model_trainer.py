# -*- coding: utf-8 -*-
# @Time    : 9/16/18 10:37
# @Author  : Lucien Cho
# @File    : model_trainer.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from categ import models
from categ import Classifier

MODELS = {
    'mlp': models.MLPClassifier,
    'cnn': models.CNNClassifier,
    'vdcnn': models.VDCNNClassifier,
    'drnn': models.DRNNClassifier
}


if __name__ == '__main__':
    model_dir = r'/Users/yaozhao/Desktop/ai_challenger/models'
    name='drnn'
    cl = Classifier(MODELS[name], model_dir, name, is_training=True)
    cl.train()
