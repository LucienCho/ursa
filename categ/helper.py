# -*- coding: utf-8 -*-
# @Time    : 9/7/18 00:10
# @Author  : Lucien Cho
# @File    : helper.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import os
import numpy as np
import random
import pandas as pd
import tensorflow as tf
from collections import Counter
import jieba_fast as jieba
import tqdm

PROJECT_HOME = r'/Users/yaozhao/Desktop/ai_challenger'

DATA_DIR = os.path.join(PROJECT_HOME, 'data')
VOCAB_DIR = os.path.join(PROJECT_HOME, 'vocab')
MODEL_DIR = os.path.join(PROJECT_HOME, 'models')

TRAINSET_PATH = os.path.join(DATA_DIR, 'sentiment_analysis_trainingset.csv')
VALIDSET_PATH = os.path.join(DATA_DIR, 'sentiment_analysis_validationset.csv')
TEST_A_PATH = os.path.join(DATA_DIR, 'sentiment_analysis_testa.csv')
INDICES_PATH = os.path.join(VOCAB_DIR, 'indices.npz')

PAD_ID = 0
UNK_ID = 1
EOS_ID = 2


class ModeKey(object):
    TRAIN = 'train'
    VALID = 'valid'
    TESTA = 'testa'


class DataUtils(object):
    def __init__(self, mode):
        self.mode = mode
        self.df = self._read_csv()

    def _read_csv(self):
        if self.mode == ModeKey.TRAIN:
            return pd.read_csv(TRAINSET_PATH)
        elif self.mode == ModeKey.VALID:
            return pd.read_csv(VALIDSET_PATH)
        elif self.mode == ModeKey.TESTA:
            return pd.read_csv(TEST_A_PATH)
        else:
            raise ValueError('Invalid mode: {}'.format(self.mode))

    @property
    def category_names(self):
        return self.df.keys().to_native_types()[2:].tolist()

    @property
    def data_size(self):
        return self.df.shape[0]

    @property
    def content(self):
        return [text[1: -1] for text in self.df.content]

    @staticmethod
    def maybe_mkdir(directory):
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory

    def shuffle(self):
        random.shuffle(self.content)


class DataViewer(DataUtils):
    def __init__(self, mode):
        super(DataViewer, self).__init__(mode)

    def view_categ_distribution(self):
        factor = 100 / self.data_size
        print('-- ' * 30)
        print('Categorical Distribution:')
        print('{}\t{}\t{}\t{}\t{}'.format(
            'category_name'.rjust(45),
            '-1'.rjust(7),
            '0'.rjust(7),
            '1'.rjust(7),
            '-2'.rjust(7)))
        for c_name in self.category_names:
            counter = Counter(self.df[c_name].values)
            print('{}\t{}\t{}\t{}\t{}'.format(
                c_name.rjust(45),
                '{:.2f}%'.format(counter[-1] * factor).rjust(7),
                '{:.2f}%'.format(counter[0] * factor).rjust(7),
                '{:.2f}%'.format(counter[1] * factor).rjust(7),
                '{:.2f}%'.format(counter[-2] * factor).rjust(7)))

    def view_length_distribution(self):
        counter = Counter([len(text) for text in self.content])
        count = 0
        interval = 100
        print('-- ' * 30)
        print('Length Distribution:')
        for i in range(100, 2100):
            count += counter.get(i, 0)
            if not i % 100:
                print('{}\t{}'.format(
                    '{}~{}'.format(i - interval + 1, i).rjust(20),
                    '{:.2f}%'.format(count * 100 / self.data_size).rjust(7)
                ))
                count = 0


class DataLoader(DataUtils):
    def __init__(self, mode, params=None):
        super(DataLoader, self).__init__(mode)
        self.params = params
        if os.path.exists(INDICES_PATH):
            self.indices = self.load_indices()
        else:
            self.indices = self.build_indices()

    @staticmethod
    def _indices(x, y, z):
        return {'x2id': {i: n for n, i in enumerate(x)},
                'id2x': {n: i for n, i in enumerate(x)},
                'y2id': {i: n for n, i in enumerate(y)},
                'id2y': {n: i for n, i in enumerate(y)},
                'z2id': {i: n for n, i in enumerate(z)},
                'id2z': {n: i for n, i in enumerate(z)}}

    def load_indices(self):
        indices_npz = np.load(INDICES_PATH)
        x = indices_npz['x']
        y = indices_npz['y']
        z = indices_npz['z']
        return self._indices(x, y, z)

    def build_indices(self):
        self.maybe_mkdir(VOCAB_DIR)
        x_counter = dict()
        for line in tqdm.tqdm(self.content):
            toks = jieba.lcut(line)
            for t in toks:
                if t not in x_counter:
                    x_counter[t] = 1
                else:
                    x_counter[t] += 1
        x = dict()
        for c in x_counter.keys():
            if x_counter[c] > 1:
                x[c] = x_counter[c]
        x = sorted(x, key=x.get, reverse=True)
        y = [-1, 0, 1, 2]
        z = self.category_names
        np.savez(INDICES_PATH, x=x, y=y, z=z)
        return self._indices(x, y, z)

    def encode_x(self, xs):
        xs = jieba.lcut(xs)
        xs = [self.indices['x2id'].get(x, UNK_ID) for x in xs]
        xs = xs[: self.params.max_len] + [PAD_ID] * (self.params.max_len - len(xs))
        return xs

    def _next_idx(self, idx, batch_size):
        start = idx % self.data_size
        if start + batch_size > self.data_size:
            selections = list(range(start, self.data_size))
            selections += list(range(0, (start + batch_size) % self.data_size))
            flag = True
        else:
            selections = list(range(start, start + batch_size))
            flag = False
        return selections, idx + batch_size, flag

    def batch_iter(self, batch_size, num_epochs=100000):
        all_xs = [self.encode_x(i[1: -1]) for i in self.df.content]
        all_ys = self.df.iloc[:, 2:].values
        epoch = 1
        idx = 0
        while epoch <= num_epochs:
            ids, idx, update_epoch = self._next_idx(idx, batch_size)
            xs = [all_xs[i] for i in ids]
            ys = [all_ys[i] for i in ids]
            if update_epoch:
                epoch += 1
                print('Start epoch: {}'.format(epoch))
            yield np.array(xs), np.array(ys) + 2


def get_params():
    indices_npz = np.load(INDICES_PATH)
    x = indices_npz['x']
    y = indices_npz['y']
    z = indices_npz['z']
    return tf.contrib.training.HParams(
        output_size=len(z),
        vocab_size=len(x))
