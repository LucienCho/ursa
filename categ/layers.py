# -*- coding: utf-8 -*-
# @Time    : 9/16/18 09:46
# @Author  : Lucien Cho
# @File    : layers.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import tensorflow as tf
from tensorflow.python.ops import array_ops


def focal_loss(prediction_tensor, target_tensor, alpha=0.25, gamma=2):
    r"""Compute focal loss for predictions.
        Multi-labels Focal loss formula:
            FL = -alpha * (z-p)^gamma * log(p) -(1-alpha) * p^gamma * log(1-p)
                 ,which alpha = 0.25, gamma = 2, p = sigmoid(x), z = target_tensor.
    Args:
     prediction_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing the predicted logits for each class
     target_tensor: A float tensor of shape [batch_size, num_anchors,
        num_classes] representing one-hot encoded classification targets
     alpha: A scalar tensor for focal loss alpha hyper-parameter
     gamma: A scalar tensor for focal loss gamma hyper-parameter
    Returns:
        loss: A (scalar) tensor representing the value of the loss function
    """
    sigmoid_p = tf.nn.sigmoid(prediction_tensor)
    zeros = array_ops.zeros_like(sigmoid_p, dtype=sigmoid_p.dtype)

    # For poitive prediction, only need consider front part loss, back part is 0;
    # target_tensor > zeros <=> z=1, so poitive coefficient = z - p.
    pos_p_sub = array_ops.where(target_tensor > zeros, target_tensor - sigmoid_p, zeros)

    # For negative prediction, only need consider back part loss, front part is 0;
    # target_tensor > zeros <=> z=1, so negative coefficient = 0.
    neg_p_sub = array_ops.where(target_tensor > zeros, zeros, sigmoid_p)
    per_entry_cross_ent = - alpha * (pos_p_sub ** gamma) * tf.log(tf.clip_by_value(sigmoid_p, 1e-8, 1.0)) \
                          - (1 - alpha) * (neg_p_sub ** gamma) * tf.log(tf.clip_by_value(1.0 - sigmoid_p, 1e-8, 1.0))
    return tf.reduce_sum(per_entry_cross_ent)


def linear(tensor, size, dtype, name='linear'):
    shape = tensor.get_shape().as_list()
    assert len(shape) == 2
    assert shape[1]
    with tf.variable_scope(name):
        w = tf.get_variable(
            name='w',
            shape=[shape[1], size],
            dtype=dtype)
        b = tf.get_variable(
            name='b',
            shape=[size],
            dtype=dtype)
    return tf.nn.xw_plus_b(tensor, w, b)


def highway(tensor, size, dtype, num_layers=1,
            bias=-2.0, activation=tf.nn.relu, name='highway'):
    with tf.variable_scope(name):
        for idx in range(num_layers):
            g = activation(
                linear(tensor, size, dtype, name=("nonlinearity_{0}".format(idx))))
            t = tf.sigmoid(
                linear(tensor, size, dtype, name=("highway_gate_{0}".format(idx))) + bias)
            output = t * g + (1. - t) * tensor
            tensor = output
    return tensor
