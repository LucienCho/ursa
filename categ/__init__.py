# -*- coding: utf-8 -*-
# @Time    : 9/7/18 00:03
# @Author  : Lucien Cho
# @File    : __init__.py.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from .classifier import Classifier
