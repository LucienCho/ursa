# -*- coding: utf-8 -*-
# @Time    : 9/8/18 21:27
# @Author  : Lucien Cho
# @File    : data_builder.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from categ.helper import DataLoader, ModeKey


if __name__ == '__main__':
    dl = DataLoader(ModeKey.TRAIN)
