# -*- coding: utf-8 -*-
# @Time    : 9/8/18 21:47
# @Author  : Lucien Cho
# @File    : models.py
# @Software: PyCharm
# @Contact : luciencho@aliyun.com

from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

import tensorflow as tf

from categ import layers


class MultiLabelClassifier(object):
    def __init__(self, params=None):
        self.params = self.merge_params(params)
        self.tensors = {}

    def build_graph(self):
        self.bottom()
        self.body()
        self.top()

    def bottom(self):
        with tf.variable_scope('placeholders'):
            self.tensors['inputs'] = tf.placeholder(
                dtype=tf.int32,
                shape=[None, self.params.max_len],
                name='inputs')
            self.tensors['labels'] = tf.placeholder(
                dtype=self.params.dtype,
                shape=[None, self.params.output_size],
                name='labels')
            self.tensors['keep_prob'] = tf.placeholder(
                dtype=self.params.dtype,
                shape=None,
                name='keep_prob')

        with tf.variable_scope('embedding'), tf.device('/cpu:0'):
            self.tensors['embeddings'] = tf.get_variable(
                name='embeddings',
                shape=[self.params.vocab_size, self.params.emb_dim],
                dtype=self.params.dtype)
            self.tensors['inputs_emb'] = tf.nn.embedding_lookup(
                self.tensors['embeddings'], self.tensors['inputs'])

    def body(self):
        raise NotImplementedError

    def top(self):
        with tf.variable_scope("logits"):
            self.tensors['w'] = tf.get_variable(
                name='w',
                shape=[self.tensors['body_outputs'].get_shape()[-1],
                       self.params.output_size * 4],
                dtype=self.params.dtype,
                initializer=tf.contrib.layers.xavier_initializer())
            self.tensors['b'] = tf.get_variable(
                name='b',
                shape=[self.params.output_size * 4],
                dtype=self.params.dtype,
                initializer=tf.constant_initializer(0.1))
            logits = tf.nn.xw_plus_b(
                x=self.tensors['body_outputs'],
                weights=self.tensors['w'],
                biases=self.tensors['b'])
            logits = tf.reshape(logits, [-1, self.params.output_size, 4])
            self.tensors['logits'] = tf.nn.softmax(logits, axis=-1)
            self.tensors['predictions'] = tf.argmax(self.tensors['logits'], axis=-1)

        with tf.variable_scope('loss'):
            # losses = layers.focal_loss(
            #     logits, tf.one_hot(tf.cast(self.tensors['labels'], tf.int32), 1))
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
                labels=tf.cast(self.tensors['labels'], tf.int32),
                logits=logits)
            losses = tf.reduce_mean(
                input_tensor=losses,
                name='sigmoid_losses')
            l2_losses = tf.nn.l2_loss(self.tensors['w'])
            l2_losses += tf.nn.l2_loss(self.tensors['b'])
            self.tensors['loss'] = tf.add(
                x=losses,
                y=l2_losses * self.params.l2_lambda,
                name='loss')

        with tf.variable_scope('metrics'):
            self.tensors['accuracy'] = tf.metrics.accuracy(
                self.tensors['labels'], self.tensors['predictions'])

    def merge_params(self, params):
        default_params = self.default_params()
        if params is not None:
            new_params = tf.contrib.training.HParams()
            for k in default_params.values():
                new_params.add_hparam(k, default_params.get(k))
            for k in params.values():
                new_params.add_hparam(k, params.get(k))
        else:
            new_params = default_params
        for k in new_params.values():
            print('Attribute key: {}value: [{}]'.format(
                k.ljust(15), str(new_params.get(k))))
        return new_params

    @staticmethod
    def default_params():
        return tf.contrib.training.HParams(
            batch_size=16,
            num_epochs=10,
            show_step=10,
            save_step=100,
            emb_dim=128,
            max_len=1000,
            keep_prob=0.5,
            learning_rate=1e-3,
            decay_rate=0.99,
            l2_lambda=1e-6,
            dtype=tf.float32)


class MLPClassifier(MultiLabelClassifier):
    def __init__(self, params):
        super(MLPClassifier, self).__init__(params)
        self.build_graph()

    def body(self):
        with tf.variable_scope('mlp'):
            x_emb = tf.expand_dims(self.tensors['inputs_emb'], axis=-1)
            seq_lens = tf.reduce_sum(tf.sign(self.tensors['inputs']), axis=1)
            seq_lens = tf.cast(seq_lens, dtype=tf.int32)
            seq_mask = tf.sequence_mask(seq_lens, self.params.max_len)

            seq_mask = tf.expand_dims(tf.cast(seq_mask, self.params.dtype), axis=-1)
            seq_mask = tf.expand_dims(seq_mask, axis=-1)  # batch L 1 1

            # Average pooling
            x_sum = tf.multiply(x_emb, seq_mask)  # batch L emb 1
            h_enc = tf.reduce_sum(x_sum, axis=1, keep_dims=True)  # batch 1 emb 1
            h_enc = tf.squeeze(h_enc, [1, 3])  # batch emb
            x_mask_sum = tf.reduce_sum(seq_mask, axis=1, keep_dims=True)  # batch 1 1 1
            x_mask_sum = tf.squeeze(x_mask_sum, [2, 3])  # batch 1
            h_enc_1 = h_enc / x_mask_sum  # batch emb

            # Maximum pooling
            h_enc_2 = tf.nn.max_pool(x_emb, [1, self.params.max_len, 1, 1], [1, 1, 1, 1], 'VALID')
            h_enc_2 = tf.squeeze(h_enc_2, [1, 3])

            h_enc = tf.concat([h_enc_1, h_enc_2], 1)

            highway_outputs = layers.highway(
                tensor=h_enc,
                size=h_enc.get_shape()[1],
                dtype=self.params.dtype,
                num_layers=1,
                bias=0)
            self.tensors['body_outputs'] = tf.nn.dropout(
                x=highway_outputs,
                keep_prob=self.tensors['keep_prob'])


class CNNClassifier(MultiLabelClassifier):
    def __init__(self, params):
        super(CNNClassifier, self).__init__(params)
        self.build_graph()

    def body(self):
        with tf.variable_scope('cnn'):
            x_emb = tf.expand_dims(self.tensors['inputs_emb'], axis=-1)
            pooled_outputs = []
            for i, filter_size in enumerate(self.params.filter_sizes):
                with tf.name_scope("conv-maxpool-%s" % filter_size):
                    filter_shape = [filter_size, self.params.emb_dim, 1, self.params.num_filters]
                    w = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="w")
                    b = tf.Variable(tf.constant(0.1, shape=[self.params.num_filters]), name="b")
                    conv = tf.nn.conv2d(
                        x_emb,
                        w,
                        strides=[1, 1, 1, 1],
                        padding="VALID",
                        name="conv")
                    h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                    pooled = tf.nn.max_pool(
                        h,
                        ksize=[1, self.params.max_len - filter_size + 1, 1, 1],
                        strides=[1, 1, 1, 1],
                        padding='VALID',
                        name="pool")
                    pooled_outputs.append(pooled)
            num_filters_total = self.params.num_filters * len(self.params.filter_sizes)
            h_pool = tf.concat(pooled_outputs, 3)
            h_pool_flat = tf.reshape(h_pool, [-1, num_filters_total])
            h_pool_flat_hw = layers.highway(
                tensor=h_pool_flat,
                size=h_pool_flat.get_shape()[1],
                dtype=self.params.dtype,
                num_layers=1,
                bias=0)
            self.tensors['body_outputs'] = tf.nn.dropout(h_pool_flat_hw, self.tensors['keep_prob'])

    @staticmethod
    def default_params():
        return tf.contrib.training.HParams(
            batch_size=16,
            num_epochs=10,
            show_step=10,
            save_step=100,
            emb_dim=128,
            max_len=1000,
            keep_prob=0.5,
            learning_rate=1e-3,
            decay_rate=0.99,
            l2_lambda=1e-6,
            dtype=tf.float32,
            filter_sizes=[3, 5, 7],
            num_filters=256)


class VDCNNClassifier(MultiLabelClassifier):
    def __init__(self, params):
        super(VDCNNClassifier, self).__init__(params)
        self.build_graph()

    def conv_block(self, inputs, i, is_training, max_pool=True):
        conv = None
        with tf.variable_scope("conv-block-%s" % i):
            for j in range(2):
                with tf.variable_scope("conv-%s" % j):
                    conv = tf.layers.conv2d(
                        inputs,
                        filters=self.params.num_filters[i],
                        kernel_size=[self.params.filter_sizes[i], self.params.num_filters[i - 1]],
                        kernel_initializer=tf.keras.initializers.he_normal(),
                        activation=None)
                    # batch normalization
                    conv = tf.layers.batch_normalization(conv, training=is_training)
                    # relu
                    conv = tf.nn.relu(conv)
                    conv = tf.transpose(conv, [0, 1, 3, 2])

            if max_pool:
                # Max pooling
                pool = tf.layers.max_pooling2d(
                    conv,
                    pool_size=(3, 1),
                    strides=(2, 1),
                    padding="SAME")
                return pool
            else:
                return conv

    def body(self):
        with tf.variable_scope('cnn'):
            input_x_ = tf.expand_dims(self.tensors['inputs_emb'], -1)
            is_training = tf.less(self.tensors['keep_prob'], 1.0)

            with tf.variable_scope("conv-0"):
                conv0 = tf.layers.conv2d(
                    input_x_,
                    filters=self.params.num_filters[0],
                    kernel_size=[self.params.filter_sizes[0], self.params.emb_dim],
                    kernel_initializer=tf.keras.initializers.he_normal(),
                    activation=tf.nn.relu)
                conv0 = tf.transpose(conv0, [0, 1, 3, 2])

            with tf.name_scope("conv-block-1"):
                conv1 = self.conv_block(conv0, 1, is_training)

            with tf.name_scope("conv-block-2"):
                conv2 = self.conv_block(conv1, 2, is_training)

            with tf.name_scope("conv-block-3"):
                conv3 = self.conv_block(conv2, 3, is_training)

            with tf.name_scope("conv-block-4"):
                conv4 = self.conv_block(conv3, 4, is_training, max_pool=False)

            with tf.name_scope("k-max-pooling"):
                h = tf.transpose(tf.squeeze(conv4, -1), [0, 2, 1])
                top_k = tf.nn.top_k(h, k=8, sorted=False).values
                h_flat = tf.reshape(top_k, [-1, 512 * 8])

            with tf.name_scope("fc-1"):
                fc1_out = tf.layers.dense(
                    h_flat, 2048, activation=tf.nn.relu,
                    kernel_initializer=tf.truncated_normal_initializer(stddev=0.05))

            with tf.name_scope("fc-2"):
                fc2_out = tf.layers.dense(
                    fc1_out, 2048, activation=tf.nn.relu,
                    kernel_initializer=tf.truncated_normal_initializer(stddev=0.05))

            self.tensors['body_outputs'] = tf.nn.dropout(fc2_out, self.tensors['keep_prob'])

    @staticmethod
    def default_params():
        return tf.contrib.training.HParams(
            batch_size=16,
            num_epochs=10,
            show_step=10,
            save_step=100,
            emb_dim=16,
            max_len=1000,
            keep_prob=0.5,
            learning_rate=1e-3,
            decay_rate=0.99,
            l2_lambda=1e-6,
            dtype=tf.float32,
            filter_sizes=[3, 3, 3, 3, 3],
            num_filters=[64, 64, 128, 256, 512],
            num_rep_block=[2, 2, 2, 2],
            pooling_filter_size=2,
            top_k_max_pooling_size=8,
            hidden_size=2048)


class DRNNClassifier(MultiLabelClassifier):
    def __init__(self, params):
        super(DRNNClassifier, self).__init__(params)
        self.build_graph()

    def body(self):
        with tf.variable_scope('drnn'):
            pad_inputs = tf.pad(
                self.tensors['inputs_emb'],
                [[0, 0], [self.params.window_size - 1, 0], [0, 0]], mode='CONSTANT')
            rnn_inputs = []
            for i in range(self.params.max_len):
                rnn_inputs.append(tf.slice(
                    pad_inputs, [0, i, 0], [-1, self.params.window_size, -1], name='rnn_input'))
            rnn_input_tensor = tf.concat(rnn_inputs, 1)

            cell = tf.nn.rnn_cell.GRUCell(self.params.hidden_size)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, self.tensors['keep_prob'])
            outputs, state = tf.nn.dynamic_rnn(cell, rnn_input_tensor, dtype=self.params.dtype)
            output_stack = tf.stack(outputs, axis=0)
            output_trans = tf.transpose(output_stack, [1, 0, 2])
            output_unstack = tf.unstack(output_trans, axis=0)

            outputs_list = []
            w = tf.get_variable('w', [self.params.hidden_size, self.params.mlp_size],
                                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.get_variable('b', [self.params.mlp_size],
                                initializer=tf.constant_initializer(0.1))
            for i in range(len(output_unstack)):
                if (i + 1) % 3 == 0:
                    output = tf.nn.relu(tf.add(tf.matmul(output_unstack[i], w), b))
                    output = tf.nn.top_k(output, 1)[0]
                    outputs_list.append(output)
            output = tf.concat(outputs_list, 1)
            self.tensors['body_outputs'] = tf.nn.dropout(output, self.tensors['keep_prob'])

    @staticmethod
    def default_params():
        return tf.contrib.training.HParams(
            batch_size=16,
            num_epochs=10,
            show_step=10,
            save_step=100,
            emb_dim=128,
            max_len=1000,
            keep_prob=0.5,
            learning_rate=1e-3,
            decay_rate=0.99,
            l2_lambda=1e-6,
            dtype=tf.float32,
            window_size=3,
            hidden_size=256,
            mlp_size=256)
